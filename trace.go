package tracing

import (
	"context"
	"fmt"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.9.0"
)

// InitTracer prepare OpenTelemetry tracer.
//
// Example:
//       package main
//
//       import (
//       	"context"
//       	"fmt"
//       	"os"
//       	"time"
//
//       	"github.com/rs/zerolog"
//
//       	tt "gitlab.com/czjopi/go-tracing-example"
//       )
//
//       const (
//       	samplingRate       = 1.0
//       	serviceName        = "example-app"
//       	serviceVersion     = "0.1"
//          serviceEnvironment = "local"
//       	traceEndpoint      = "http://tempo-distributor.tracing:4317"
//       	traceRate          = 1.0
//       )
//
//      func main() {
//      	zerolog.TimeFieldFormat = time.RFC3339Nano
//      	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()
//
//      	logger.Info().Msgf("starting service %s", serviceName)
//
//      	ctx := context.Background()
//
//          prepareTracer := tt.NewTracer(traceEndpoint, traceRatio, serviceName, serviceVersion, serviceName)
//	        tp, err := prepareTracer.Start(ctx)
//      	if err != nil {
//      		logger.Fatal().Err(err).Msg("failed to initialize tracer")
//      	}
//      	defer func() {
//      		if err := prepareTracer.Stop(ctx); err != nil {
//      			logger.Fatal().Err(err).Msg("failed to shutdown TracerProvider")
//      		}
//      	}()
//      	tracer := tp.Tracer("main")
//
//      	ctx, span := tracer.Start(ctx, "run server")
//      	logger.Printf("traceid: %s, spanid: %s", span.SpanContext().TraceID(), span.SpanContext().SpanID())
//      	defer span.End()
//
//      	logger.Info().Msgf("service %s finished", serviceName)
//      }

type Tracer struct {
	endpointURL        string
	samplingRate       float64
	serviceName        string // app name
	serviceVersion     string
	serviceEnvironment string // local
	provider           *sdktrace.TracerProvider
}

// NewTracer creates new Tracer.
func NewTracer(
	endpointURL string,
	samplingRate float64,
	serviceName string,
	serviceVersion string,
	serviceEnvironment string) *Tracer {
	return &Tracer{
		endpointURL:        endpointURL,
		samplingRate:       samplingRate,
		serviceName:        serviceName,
		serviceVersion:     serviceVersion,
		serviceEnvironment: serviceEnvironment}
}

// Start creates OpenTelemetry:
// - resource
// - grpc exporter
// - provider with sampling
// - registers created provider for tracing
// and setup trace propagation.
func (t *Tracer) Start(ctx context.Context) (provider *sdktrace.TracerProvider, err error) {
	// resource
	res, err := resource.New(
		ctx,
		resource.WithAttributes(
			semconv.ServiceNameKey.String(t.serviceName),
			semconv.ServiceVersionKey.String(t.serviceVersion),
			semconv.DeploymentEnvironmentKey.String(t.serviceEnvironment),
			semconv.TelemetrySDKLanguageGo,
		),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create resource: %w", err)
	}

	// exporter
	exporter, err := otlptrace.New(
		ctx,
		otlptracegrpc.NewClient(
			otlptracegrpc.WithInsecure(),
			otlptracegrpc.WithEndpoint(t.endpointURL),
		),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create exporter: %w", err)
	}

	// provider
	t.provider = sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.ParentBased(sdktrace.TraceIDRatioBased(t.samplingRate))),
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(res),
	)

	// set tracer provider
	otel.SetTracerProvider(t.provider)

	// set global propagator to TraceContext (the default is no-op).
	otel.SetTextMapPropagator(propagation.TraceContext{})

	return t.provider, nil
}

// Stop immediately exports all not exported spans and shutdown provider,
// which stops the whole tracer.
func (t *Tracer) Stop(ctx context.Context) error {
	return t.provider.Shutdown(ctx)
}
