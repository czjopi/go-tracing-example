# Basic example

## Run required components (grafana, tempo)
```
docker compose -f docker-compose.yml up -d
```

## Run basic example app
```
go run main.go
```

## Get trace id

1. from app output
```
{"level":"info","time":"2023-06-06T17:08:51.204191+02:00","message":"starting service example-app"}
{"level":"debug","time":"2023-06-06T17:08:51.205459+02:00","message":"traceid: f4a08e8911485d82885cf8a7199f64ed, spanid: 9047abe0e8ae38db"}
{"level":"info","time":"2023-06-06T17:08:51.205471+02:00","message":"service example-app finished"}
```

2. or from tempo logs
```
docker logs examples-tempo-1 -n 10
```

## View trace

Go to [Grafana/Explore](http://localhost:3000/explore) select __Tempo__ as source and put [trace id](#get-trace-id) to TraceID Query type colon. Or use Search Query type.
