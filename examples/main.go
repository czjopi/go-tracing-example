package main

import (
	"context"
	"os"
	"time"

	"github.com/rs/zerolog"

	tt "gitlab.com/czjopi/go-tracing-example"
)

var (
	serviceEnvironment = "local"
	serviceName        = "example-app"
	serviceVersion     = "0.1"
	traceRatio         = 1.0
	traceEndpoint      = "localhost:4317"
)

func main() {
	zerolog.TimeFieldFormat = time.RFC3339Nano
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	logger.Info().Msgf("starting service %s", serviceName)

	ctx := context.Background()

	prepareTracer := tt.NewTracer(traceEndpoint, traceRatio, serviceName, serviceVersion, serviceEnvironment)
	tp, err := prepareTracer.Start(ctx)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize tracer")
	}
	defer func() {
		if err := prepareTracer.Stop(ctx); err != nil {
			logger.Fatal().Err(err).Msg("failed to shutdown tracer")
		}
	}()
	tracer := tp.Tracer("main")

	ctx, span := tracer.Start(ctx, "run server")
	logger.Printf("traceid: %s, spanid: %s", span.SpanContext().TraceID(), span.SpanContext().SpanID())
	defer span.End()

	logger.Info().Msgf("service %s finished", serviceName)
}
