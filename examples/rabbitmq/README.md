# Basic RabbitMQ example

## Run required components
```
docker-compose -f ../docker-compose.yml -f ../docker-compose-rabbitmq.yml up -d
```

## Run consumer
```
go run consume.go info warning error
```

## Publish message
```
go run publish.go
```
or

```
go run publish.go [info] [warning] [error] message
```

## Get trace id

From tempo log
```
docker logs examples_tempo_1 -n 10
```

## View trace

Go to [Grafana/Explore](http://localhost:3000/explore) select __Tempo__ as source and put [trace id](#get-trace-id) to TraceID Query type colon. Or use Search Query type.
