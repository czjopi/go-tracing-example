package main

import (
	"context"
	"flag"
	"os"
	"time"

	"github.com/rs/zerolog"

	amqp "github.com/rabbitmq/amqp091-go"

	tt "gitlab.com/czjopi/go-tracing-example"
)

var (
	serviceEnvironment = "local"
	serviceName        = "log_publisher"
	serviceVersion     = "0.1"
	traceEndpoint      = "localhost:4317"
	traceRatio         = 1.0
)

func main() {
	zerolog.TimeFieldFormat = time.RFC3339Nano
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	var severity, body string
	flag.StringVar(&severity, "severity", "info", "severity of message")
	flag.StringVar(&body, "body", "hello", "body of message")

	flag.Parse()

	logger.Info().Msgf("starting service %s", serviceName)

	ctx := context.Background()

	prepareTracer := tt.NewTracer(traceEndpoint, traceRatio, serviceName, serviceVersion, serviceEnvironment)
	tp, err := prepareTracer.Start(ctx)
	if err != nil {
		logger.Fatal().Err(err).Msgf("failed to initialize tracer")
	}
	defer func() {
		if err := prepareTracer.Stop(ctx); err != nil {
			logger.Fatal().Err(err).Msgf("failed to shutdown TracerProvider")
		}
	}()
	tracer := tp.Tracer("examples/rabbitmq/publish")

	ctx, span := tracer.Start(ctx, "run publisher")
	defer span.End()

	ctx, span = tracer.Start(ctx, "connecting to rabbitmq")
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to connect to RabbitMQ")
	}
	span.End()
	defer conn.Close()

	ctx, span = tracer.Start(ctx, "connecting to a channel")
	ch, err := conn.Channel()
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to open a channel")
	}
	span.End()
	defer ch.Close()

	ctx, span = tracer.Start(ctx, "declare exchange")
	err = ch.ExchangeDeclare(
		"logs",   // name
		"direct", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to declare an exchange")
	}
	span.End()

	ctx, span = tracer.Start(ctx, "publishing message")
	err = ch.Publish(
		"logs",   // exchange
		severity, // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to publish a message")
	}
	span.End()

	logger.Printf(" [x] Sent %s", body)
}
