package main

import (
	"context"
	"flag"
	"os"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
	tt "gitlab.com/czjopi/go-tracing-example"
)

var (
	serviceEnvironment = "local"
	serviceName        = "log_consumer"
	serviceVersion     = "0.1"
	traceRatio         = 1.0
	traceEndpoint      = "localhost:4317"
)

func main() {
	zerolog.TimeFieldFormat = time.RFC3339Nano
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	flag.Parse()

	queries := flag.Args()
	if len(queries) == 0 {
		logger.Print("Usage: consume [info] [warning] [error]")
		os.Exit(1)
	}

	logger.Info().Msgf("starting service %s", serviceName)

	ctx := context.Background()

	prepareTracer := tt.NewTracer(traceEndpoint, traceRatio, serviceName, serviceVersion, serviceEnvironment)
	tp, err := prepareTracer.Start(ctx)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to initialize tracer")
	}
	defer func() {
		if err := prepareTracer.Stop(ctx); err != nil {
			logger.Fatal().Err(err).Msg("failed to shutdown tracer")
		}
	}()
	tracer := tp.Tracer("examples/rabbitmq/receive")

	ctx, span := tracer.Start(ctx, "run consumer")
	defer span.End()

	ctx, span = tracer.Start(ctx, "connecting to rabbitmq")
	connection, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to connect to RabbitMQ")
	}
	defer connection.Close()
	defer span.End()

	ctx, span = tracer.Start(ctx, "open channel")
	channel, err := connection.Channel()
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to open a channel")
	}
	defer channel.Close()
	defer span.End()

	ctx, span = tracer.Start(ctx, "declaring channel")
	err = channel.ExchangeDeclare(
		"logs",   // name
		"direct", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to declare an exchange")
	}
	span.End()

	ctx, span = tracer.Start(ctx, "declaring queue")
	q, err := channel.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to declare a queue")
	}
	span.End()

	for _, s := range queries {
		logger.Printf("Binding queue %s to exchange %s with routing key %s", q.Name, "logs", s)
		err = channel.QueueBind(
			q.Name, // queue name
			s,      // routing key
			"logs", // exchange
			false,
			nil)
		if err != nil {
			logger.Fatal().Err(err).Msg("failed to bind a queue")
		}
	}

	msgs, err := channel.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		false,  // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)
	if err != nil {
		logger.Fatal().Err(err).Msg("failed to register a consumer")
	}

	var forever chan struct{}

	go func() {
		for d := range msgs {
			logger.Printf(" [x] %s", d.Body)
		}
	}()

	logger.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}
